TODO
====

Mikini's loose notes about big and small changes to do.

High priority
-------------

* [DONE in v1.6.0](https://gitlab.com/OSM-DK/autoAWS/-/commit/af82111b61350226aafd54e61aaa22ffdcccd973): use OAuth 2.0 authentication against OSM API due to [deprecation of HTTP Basic Auth support](https://www.openstreetmap.org/user/pnorman/diary/401157)!
    * **Shutdown effective from [2024-07-01 20:04 CEST](https://github.com/openstreetmap/chef/commit/8dd0893b2c8f8996079a6aa4288539b6ba1cd896)** (last autoAWS Basic Auth request performed on [update of postcode 9800 @ 2024-07-01 10:10:23 CEST](https://www.openstreetmap.org/changeset/153409274), first failure on postcode 2150 @ 2024-07-01 23:24:02 CEST)
    * Maybe use native PHP implementation https://git.sr.ht/~fkooman/php-oauth2-client or https://oauth2-client.thephpleague.com/ (OSM: https://github.com/jbelien/oauth2-openstreetmap)
    * a non-expiring bearer token ([RFC6759](https://datatracker.ietf.org/doc/html/rfc6750)) seems to be supported and adequate, so something [like this python code](https://github.com/metaodi/osmapi/blob/develop/examples/oauth2.py) could be used to generate a token as a one-time procedure at deployment

Operation/deployment
--------------------

* [DONE in v1.6.0](https://gitlab.com/OSM-DK/autoAWS/-/commit/6c0495055cf3cae97571b00d36f680d1ca6cd721): improve logging
    * make log file number configurable, currently hardcoded to 2
    * make log file rotation size configurable, currently hardcoded to 1'000'000 bytes
    * find out meaning behind seemingly random log file name (j8y5ocu8nk), change to something more sensible

Tagging/data
------------

* flag an address node as being overriden by oisfixes, maybe add a reference to the oisfixes entry

* flag an address node if name in DAR has substantially changed from the original when being overriden by oisfixes, see [changeset 127660394](https://www.openstreetmap.org/changeset/127660394)

* periodically notify a user (and maybe public comment on changeset?) who added autoaws=ignore to an address node to seek consensus with DAR authority about the change which prompted the need to ignore DAR, see changesets [136040329](https://www.openstreetmap.org/changeset/136040329), [133232364](https://www.openstreetmap.org/changeset/133232364)

* automatically correct "source" value if it contains an obsolete bot source (see manual correction of "OSAK (2010)" to DAR done in fx. [changeset 119819552](https://www.openstreetmap.org/changeset/119819552))

* use [Overpass API's timestamp_osm_base](https://dev.overpass-api.de/output_formats.html#xml.date) to assess whether it is safe to perform updates at all, see [discussion in changeset 89310619 about lagging Overpass causing double addresses](https://www.openstreetmap.org/changeset/89310619)

* understand why some users perform bulk address relocations with the deliberate purpose to have autoAWS move them back, their needs ought to be addressed in other ways than generating redundant changes, see [CS 103935530](https://www.openstreetmap.org/changeset/103935530), [CS 119811863](https://www.openstreetmap.org/changeset/119811863)

Miscellaneous
-------------

* adhere to [link in comment requirement in AE CoC](https://wiki.openstreetmap.org/wiki/Automated_Edits_code_of_conduct#Execute_with_caution): "You must link to the wiki page or user page documenting your changes from changeset, for example using description=* changeset tag" (maybe ask OSMF about this, tracability is obvious when uploads are done by a dedicated user)
