#! /usr/bin/python3

from requests_oauthlib import OAuth2Session
import json
import os

client_credentials_file = "oauth2_client_credentials.json"
output_file = "osm_api_oauth2_token.php"

if os.path.exists(client_credentials_file):
    with open(client_credentials_file, "r") as ccfp:
        cc = json.load(ccfp);

    if cc['client_id'] == "" or cc['client_secret'] == "" or not 'client_name' in cc:
        print("OAuth2 application data are missing from " + client_credentials_file);
        exit()
else:
    print("OAuth2 application credentials are needed in \"" + client_credentials_file + "\"");
    exit()

print("Generating OAuth2 token for application \"" + cc['client_name'] + "\"", end="")
print(" (" + cc['client_info'] +")", end="") if 'client_info' in cc else None;
print(".")

if os.path.exists(output_file):
    confirm = input("\nThis will overwrite existing token file \"" + output_file + "\". Continue? (y/N) ")
    if confirm != "y":
        exit()

redirect_uri = 'urn:ietf:wg:oauth:2.0:oob'
scope = ["write_api"]

oauth = OAuth2Session(client_id=cc['client_id'], redirect_uri=redirect_uri, scope=scope)
authorization_url, state = oauth.authorization_url('https://www.openstreetmap.org/oauth2/authorize')

print(f"\nVisit the OSM OAuth2 authentication link below in a browser, authorize and paste the resulting code here.\n\n{authorization_url}\n")

code = input("Authorization code from OSM website: ").lstrip('\n').rstrip('\n')
# weed out leading/trailing newlines breaking input when copy-pasting
# (experienced using firefox when selecting by double click and copying)
while code == '':
    code = input().lstrip('\n').rstrip('\n')

token = oauth.fetch_token('https://www.openstreetmap.org/oauth2/token',
        code=code,
        client_secret=cc['client_secret'])

print("\nWriting generated token to \"" + output_file + "\" for application to consume.")
with open(output_file, "w") as f:
    f.write("<?php\n");
    f.write("define(\"OSM_API_OAUTH2_TOKEN\", \""         + str(token['access_token']) + "\");\n")
    f.write("define(\"OSM_API_OAUTH2_TOKEN_TYPE\", \""    + str(token['token_type'])   + "\");\n")
    f.write("define(\"OSM_API_OAUTH2_TOKEN_SCOPE\", \""   + str(token['scope'])        + "\");\n")
    f.write("define(\"OSM_API_OAUTH2_TOKEN_CREATED\", \"" + str(token['created_at'])   + "\");\n")
    f.write("?>\n")
