autoAWS - a Danish OpenStreetMap import bot
===========================================

autoAWS is an application which imports data into [OpenStreetMap][osm] (OSM) from the official Danish (as in the Nordic country [Denmark][wiki-dk]) address register [Danmarks Adresser (DAR)][dar].

The official instance of this bot operated by the Danish OSM community uploads as the OSM user [autoAWS][autoaws-osm], see more at the [autoAWS][autoaws-wiki] & [bot][bot-wiki] wiki pages.

[osm]: https://openstreetmap.org/
[wiki-dk]: https://en.wikipedia.org/wiki/Denmark
[dar]: https://danmarksadresser.dk/
[autoaws-osm]: https://www.openstreetmap.org/user/autoAWS/
[autoaws-wiki]: https://wiki.openstreetmap.org/wiki/AutoAWS
[bot-wiki]: https://wiki.openstreetmap.org/wiki/Bot


Utilized services
=================

* [DAR's DAWA API][dawa]
* [OSM's v0.6 API][osm-api]
* [Overpass API][opass] ([OSM wiki][opass-wiki])
* [OIS fixes (OIS-rettelser)][oisfixes]

[dawa]: https://dawadocs.dataforsyningen.dk/dok/api
[osm-api]: https://wiki.openstreetmap.org/wiki/API_v0.6
[opass]: https://overpass-api.de/
[opass-wiki]: https://wiki.openstreetmap.org/wiki/Overpass_API
[oisfixes]: https://oisfixes.iola.dk/

Requirements
============

autoAWS is implemented as a set of PHP scripts which should run happily on [PHP][php] 7.2 and above (has been running on 8.1 until 2023-09-27). It requires installation on a system which can interpret PHP and where the PHP installation provides the ability to connect to a MySQL compatible database through the [PDO_MYSQL driver][php-mysql], has the PHP extension [SimpleXML][php-simplexml] available for doing XML manipulation, and the [cURL library][curl] and its PHP extension [Client URL Library][php-curl] enabled for OSM API access.

The above requirements can be fulfilled by installing the following packages on a [Debian based system][debian] using [APT][apt-wiki], see also [below for specific instructions](#preparations-for-running):

* php-cli
* php-mysql
* php-xml
* php-curl
* mysql-server or mariadb-server

Additionally the following are required for deployment:

* make
* python3
* python3-requests-oauthlib

[php]: https://www.php.net/
[php-mysql]: https://www.php.net/manual/en/ref.pdo-mysql.php
[php-simplexml]: https://www.php.net/manual/en/book.simplexml.php
[curl]: https://curl.se/
[php-curl]: https://www.php.net/manual/en/book.curl.php

[debian]: https://www.debian.org/
[apt-wiki]: https://en.wikipedia.org/wiki/APT_(software)


Preparations for running
========================

* Clone autoAWS repository and change to it:

        $ git clone https://gitlab.com/OSM-DK/autoAWS
        $ cd autoAWS

* Install required packages:

        $ sudo apt install php-cli php-mysql php-xml php-curl mariadb-server make python3

* Optional: install packages for continuously running in an actual deployment:

        $ sudo apt install cron

* Create ```autoaws``` database:

        $ sudo mysql <localhost.sql

* Create db user (example user ```<db user>``` with password ```<db password>```) and grant access to database:

        $ sudo mysql
        MariaDB [(none)]> create user <db user>@localhost identified by '<db password>';
        MariaDB [(none)]> grant all privileges on autoaws.* to <db user>@localhost;

* Copy the example configuration file ```config.example.php``` to ```config.php```

        $ cp config{.example,}.php

* Insert OSM API URL details in ```config.php``` define ```OSM_API_URL```  in line 3:

        $  tail +2 config.example.php|head -2
        // Connection information for OSM
        define('OSM_API_URL' , 'https://api.openstreetmap.org/api/0.6/');
        $ 

* Insert database details in ```config.php``` defines ```MYSQL_HOST```, ```MYSQL_DB```, ```MYSQL_USERNAME``` & ```MYSQL_PASSWORD``` in lines 8-11 (example from above DB user creation):

        $ tail +6 config.php |head -5
        // Connection information for MySQL/MariaDB
        define('MYSQL_HOST'    , 'localhost');
        define('MYSQL_DB'      , 'autoaws');
        define('MYSQL_USERNAME', '<db user>');
        define('MYSQL_PASSWORD', '<db password>');
        $ 

* Copy the example OAuth2 application credentials file ```oauth2_client_credentials.example.json``` to ```oauth2_client_credentials.json```

        $ cp oauth2_client_credentials.example{.example,}.php

* Create an OSM OAuth2 application by visiting [https://www.openstreetmap.org/oauth2/applications](https://www.openstreetmap.org/oauth2/applications), it should be using "Redirect URI" urn:ietf:wg:oauth:2.0:oob and having "Confidential application?" and Permission "Modify the map" ticked. Insert the generated "Client ID" and "Client Secret" in the respective fields of the ```oauth2_client_credentials.json``` file.

* Generate the OAuth2 token for OSM API access by running the script ```generate_oauth2_token.py``` and visiting the indicated URL logging in to OSM as the user for which the token should be generated:

        $ ./generate_oauth2_token.py
        Generating OAuth2 token for application "autoAWS bot" (registered 2024-07-05).
        
        Visit the OSM OAuth2 authentication link below in a browser, authorize and paste the resulting code here.
        
        https://www.openstreetmap.org/oauth2/authorize?<redacted parameters>

        Authorization code from OSM website: <redacted code>

        Writing generated token to "osm_api_oauth2_token.php" for application to consume.
        $ 

* Generate the version information file:

        $ make version.php

* Optional: setup cron jobs to run ```index.php``` and run ```cleardb.php``` periodically. Logs generated by the former are in the example configuration written to current working directory (cwd) so it needs to be changed to the wanted log location before invocation.

        $ crontab -l
        # m h  dom mon dow   command
        0-53 * * * * bash -c 'cd /home/autoaws/autoAWS; /usr/bin/php ./index.php'
        59   * * * * /usr/bin/php /home/autoaws/autoAWS/cleardb.php
        $ 


Running
=======

When index.php contains correct credentials and the database schema is in place running index.php will cause the script to pick the next post code to update addresses for and perform the update. Only one update can be processed at a time, so if a second invocation is done while one is running the second will bail out.

At the initial invocation the complete list of post codes are fetched from DAR and the complete list of name corrections are fetched from [OIS fixes/OIS-rettelser](https://oisfixes.iola.dk/). This also happens periodically during iteration through the list of post codes (which for the current 1089 post codes processed at a rate of 54 pr. hour when using the crontab above lasts roughly 20 hours) to catch any updates during this cycle.

Logging
=======

Diagnostics logging and errors from the import process are output and can be read directly on the filesystem in log files whose location and names are dictated by config.php but by default are written in current working directory and named ```autoaws.<digit>.log``` where digit is between 0 and 19. The logs are rotated when reaching the limit defined in config.php which by default is 5 MiB (ie. a total of 100 MiB logs are kept).
