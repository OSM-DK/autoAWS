<?php

require_once 'config.php';
require_once 'version.php';

error_reporting(E_ALL);
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', LOG_PATH . '/' . LOG_BASENAME . '.0.log');
ini_set('default_socket_timeout', 45);
ini_set('default_charset', 'utf-8');
ini_set('memory_limit', '512M'); 		//standard memory size not sufficient when downloading AWS data
set_time_limit(300); 					//5 minutes
define('DEBUG',			FALSE);			//disables all cURL calls to the OSM API
define('MANUAL',		FALSE);			//manually select which postcode to update
define('MANUAL_CODE',	7100);			//the postcode to update if MANUAL is TRUE
define('UPDATE_ALL',	FALSE);			//update all address nodes, even if nothing has changed. Useful when changing the tagging scheme
define('UPDATE_BY_TAG', FALSE);			//force an update of an address node if a specific tag (defined as UPDATE_TAG) is present
define('UPDATE_TAG','osak:revision');	//the tag to detect if UPDATE_BY_TAG is set to true

main();

function main(): void {
	rotate_logs();
	error_log('INFO: autoAWS ' . VERSION . " (" . VERSION_COMMIT_SHORT . ")" . ' invoked (from ' . (php_sapi_name() == "cli" ? "cli" : "http server") . ')');
	check_running_state(); //MUST be run BEFORE the shutdown function is registered
	register_shutdown_function('shutdown'); //TODO not working
	if(DEBUG) {
		error_log('INFO: DEBUG MODE ENABLED. No changes will be submitted to OSM');
	}
	if (!is_db_initialized()) {
		error_log('INFO: uninitialized database detected, populating ois_fixes and postcode tables');
		download_ois_fixes();
		opdater_postnumre();
	}
	define('POSTNUMMER', select_postcode());
    if(POSTNUMMER % 1000 == 0) { //update fixed street names and list of postcodes periodically, they do not need to be updated every time
		download_ois_fixes();
		opdater_postnumre();
	}
	download_osm_data();
	download_aws_data();
	detect_updates();
	detect_add();
	detect_delete();
	$count = (array) count_updates();
	if($count[0] == 0) { //no changes for this postcode
		database_cleanup();
		die();
	}
	split_changeset($count[0]);
	define('CHANGESET', create_changeset());
	update_nodes();
	if(CHANCE == 100) { //delete and create MUST be run in the same script execution, otherwise we might lose extra nodes
		delete_nodes();
		create_nodes();
	}
	close_changeset();
	database_cleanup();
}

## Small helper functions ##
function rotate_logs() {
	$logcommon = LOG_PATH . '/' . LOG_BASENAME . '.';
	if(filesize($logcommon . '0.log') > LOG_MAX_SIZE) {
		error_log("INFO: Rotating logs due to current log size exceeding " . LOG_MAX_SIZE . ' bytes (' . filesize($logcommon . '0.log')  . ' bytes)');
		for($logno = LOG_COUNT - 2; $logno >= 0; $logno--) { // overwrite the last log with second last
			$log = $logcommon . $logno . '.log';
			$logrot = $logcommon . ($logno + 1) . '.log';
			if(file_exists($log)) {
				rename($log, $logrot);
			}
		}
	}
}

function shutdown() { //in case of fatal error, clear the database before exiting
	error_log('INFO: Running shutdown function'); // TODO shutdown function never being called
	if ($error = error_get_last()) {
        $type_string = array_search($error['type'], get_defined_constants(true)['Core']);
        switch ($error['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
            error_log('ERROR: Fatal error'
                      . '. Type: ' . $type_string
                      . '. Message: ' . ($error['message'] ?? 'Unknown error')
                      . '. File: ' . ($error['file'] ?? 'Unknwon file')
                      . '. Line: ' . ($error['line'] ?? 'Unknown line'));
            database_cleanup(false);
            break;
        default:
            error_log('ERROR: Non-fatal error'
                      . '. Type: ' . $type_string
                      . '. Message: ' . ($error['message'] ?? 'Unknown error')
                      . '. File: ' . ($error['file'] ?? 'Unknwon file')
                      . '. Line: ' . ($error['line'] ?? 'Unknown line'));
        }
    }
}

function db_connect(): PDO {
	//connects to database using PDO and returns a PDO object
	$charset = 'utf8mb4';
	$dsn = "mysql:host=" . MYSQL_HOST . ";dbname=" . MYSQL_DB . ";charset=$charset";
	$opt = [
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		PDO::ATTR_EMULATE_PREPARES   => false,
	];
	try {
		$pdo = new PDO($dsn, MYSQL_USERNAME, MYSQL_PASSWORD, $opt);
	}
	catch(Exception $ex) {
		error_log('ERROR: Failed to connect to database');
		error_log($ex->getMessage());
		die();
	}
	return $pdo;
}

function is_db_initialized(): bool {
	// Check if multi invocation tables ois_fixes and last_update (postcodes)
	// have been initialized with contents.
	$db = db_connect();
	try {
		$row_counts = $db->query('SELECT COUNT(*) FROM ois_fixes UNION SELECT COUNT(*) FROM last_update;');
	}
	catch(Exception $ex) {
		error_log('ERROR: Failed to query ois_fixes & last_update tables');
		die();
	}
	for($i = 0; $i<$row_counts->rowCount(); $i++) {
		// regard db as uninitialized if any has zero rows
		if($row_counts->fetchColumn() == 0) {
			return false;
		}
	}
	return true;
}

function check_running_state(): void {
	//check if all dynamic database tables are empty. If not, an update might already be running, and we can't start another one.
	$db = db_connect();
	try {
		$db->exec('INSERT INTO `running_lock`(`currently_locked`) VALUES (1)'); //will fail if there is already a row in the table == another update is running (or has failed without clearing database)
	}
	catch(Exception $ex) {
		$error='(ERROR: Failed to obtain database lock)';
		error_log($error);
		print($error . "\n");
		die();
	}
	$tables = ['osmdata', 'awsdata', 'add_addr', 'delete_addr', 'update_addr', 'invalid_node', 'xmlnodes'];
	foreach($tables as $table) {
		if($db->query('SELECT * FROM `' . $table . '`')->fetch()) {
			error_log('(ERROR: Database is not empty. Aborting.)');
			die();
		}
	}
}

function add_tag(SimpleXMLElement $node, string $tags): void {
	//adds a new <tag> element to the first child of the given XML object
	$tags = explode('=', $tags);
	$tag = $node->children()[0]->addChild('tag');
	$tag->addAttribute('k', $tags[0]);
	$tag->addAttribute('v', $tags[1]);
}

function street_name(array $fixes, string $navn, string $str_no, string $mun_no): string {
	//searches the array of ois fixes for the given mun&street code
	//returns the fixed name if there is one, otherwise returns the original name
	if(empty($fixes)) {
		return $navn;
	}
	foreach($fixes as $fix) {
		if($fix['mun_no'] == $mun_no AND $fix['str_no'] == $str_no) {
			return $fix['fixed_name'];
		}
	}
	return $navn;
}

function street_name_fixes(): array {
	//loads OIS fixes from the database into an array
	$fixes = array();
	$db = db_connect();
	$stmt = $db->prepare('SELECT `fixed_name`, `mun_no`, `str_no` FROM `ois_fixes` WHERE 1');
	try {
		$stmt->execute();
	}
	catch(Exception $ex) {
		error_log('ERROR: SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	$results = $stmt->fetchAll();
	$db = NULL;
	if(!$results) { //no fix available/needed
		return $fixes; //empty array
	}
	foreach($results as $result) {
		array_push($fixes, array('mun_no' => $result['mun_no'], 'str_no' => $result['str_no'], 'fixed_name' => $result['fixed_name']));
	}
	return $fixes;
}

function id_format(string $raw): string {
	//takes a possibly ill-formatted address ID and returns it in correct format
	$string = strtolower(str_replace('-','',$raw));
	if(strlen($string) != 32) {
		return $raw; //input looks weird, gives up
	}
	$string = substr_replace($string, '-', 8, 0);
	$string = substr_replace($string, '-', 13, 0);
	$string = substr_replace($string, '-', 18, 0);
	$string = substr_replace($string, '-', 23, 0);
	if(preg_match('/^([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})$/', $string)) {
		return $string;
	}
	return $raw;
}

## Functions used to download data and work on that data in the database ##
function download_osm_data(): int {
	//via overpass API, downloads all nodes which has the osak:identifier key and the selected postcode value
	//inserts the returned JSON data into database table 'osmdata'
	//downloads the corresponding xml representation of nodes from OSM
	$string = @file_get_contents('http://overpass-api.de/api/interpreter?data=[out:json];node[%22osak:identifier%22][%22addr:postcode%22=' . POSTNUMMER . '];out;');
	if($string === FALSE) {
		error_log('ERROR: Unable to contact Overpass API (file_get_contents returned false)');
		database_cleanup(false);
		die();
	}
	if(empty($string)) { //nothing was downloaded, no addresses with the given postcode exist in OSM
		return 0;
	}
	try {
		$json = (array) json_decode($string, true);
	}
	catch(Exception $ex) {
		error_log('ERROR: Bad format of OSM data.');
		error_log($ex->getMessage());
		die();
	}
	if(!array_key_exists('elements', $json)) {
		error_log('ERROR: Bad format of downloaded OSM data (no "elements" key in array)');
		die();
	}
	$number_of_addresses = count($json['elements']);
	$number_of_errors = 0;
	unset($string);
	$db = db_connect();
	$stmt = $db->prepare('INSERT INTO `osmdata` (`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`, `autoaws_ignore`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
	$node_ids = array();
	foreach($json['elements'] AS $addr){
		$a = (string) strtoupper(str_replace('-','',$addr['tags']['osak:identifier'])); //strip osak:id for dashes and convert to uppercase. Previous import script used to do this for unknown reasons. Retained for easier backward compatability.
		$b = (int) $addr['id'];
		$c = (float) $addr['lat'];
		$d = (float) $addr['lon'];
		$e = $addr['tags']['addr:city'] ?? NULL;
		if(UPDATE_BY_TAG) {
			$e = isset($addr['tags'][UPDATE_TAG]) ? 'invalid' : $e; //by setting city name to "invalid" in the database, the DAR<=>OSM data check will fail for this address, thus triggering an update
		}
		$f = $addr['tags']['addr:country'] ?? NULL;
		$g = $addr['tags']['addr:housenumber'] ?? NULL;
		$h = $addr['tags']['addr:postcode'] ?? NULL;
		$i = $addr['tags']['addr:street'] ?? NULL;
		$j = $addr['tags']['addr:municipality'] ?? NULL;
		$k = $addr['tags']['addr:place'] ?? NULL;
		$l = 0;
		if(isset($addr['tags']['autoaws']) AND strtolower($addr['tags']['autoaws']) == 'ignore') {
			$l = 1;
			error_log('INFO: autoaws=ignore tag set on node with ID ' . $b . '. Node will be ignored.');
		}
		try {
			$stmt->execute([$a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l]);
		}
		//in case of an adress resulting in a database error, skip it and move on to the next one
		//some addresses might get skipped, but creating error handling for every scenario is almost impossible,
		//since OSM data is not validated in any way, and can contain any number of errors
		catch(PDOException $ex) {
			++$number_of_errors;
			error_log('WARNING: Unable to load an OSM node into database. Offending node ID: ' . $addr['id']);
			switch($ex->getCode()) {
				case 22001:
					delete_osm_node($b, $c, $d);
					error_log('	Invalid osak:identifier tag. Node will be deleted.');
					break;
				case 23000:
					delete_osm_node($b, $c, $d);
					error_log('	Duplicate osak:identifier tag on multiple nodes. Duplicate node will be deleted.');
					break;
				default:
					error_log($ex->getMessage());
					break;
			}
		}
		array_push($node_ids, $b);
		if(count($node_ids) > 499) { //download full XML nodes from OSM in chunks of 500
			$impl = (string) implode(',', $node_ids);
			$url = OSM_API_URL . 'nodes?nodes=' . $impl;
			try {
				$nodes = new SimpleXMLElement(file_get_contents($url));
			}
			catch(Exception $ex) {
				error_log('ERROR: Failed trying to process OSM data');
				error_log($ex->getMessage());
				database_cleanup();
				die();
			}
			foreach($nodes->node AS $child) {
				$stmt1 = $db->prepare('INSERT INTO `xmlnodes`(`node_id`, `xml`) VALUES (?,?)');
				$id = (int) $child->attributes()->id[0];
				$xml = (string) $child->asXML();
				try {
					$stmt1->execute([$id,$xml]);
				}
				catch(Exception $ex) {
					error_log('ERROR: SQL query failed');
					error_log($ex->getMessage());
					database_cleanup(false);
					die();
				}
			}
			$node_ids = array();
		}
	}
	if(!empty($node_ids)) { //download the last few nodes (in case the last chunk is not exactly 500 nodes long)	
		$impl = (string) implode(',', $node_ids);
		$url = OSM_API_URL . 'nodes?nodes=' . $impl;
		try {
			$nodes = new SimpleXMLElement(file_get_contents($url));
			}
		catch(Exception $ex) {
			error_log('ERROR: Failed trying to process remaining OSM data');
			database_cleanup();
			die();
		}
		foreach($nodes->node AS $child) {
			$stmt1 = $db->prepare('INSERT INTO `xmlnodes`(`node_id`, `xml`) VALUES (?,?)');
			$id = (int) $child->attributes()->id[0];
			$xml = (string) $child->asXML();
			try {
				$stmt1->execute([$id,$xml]);
			}
			catch(Exception $ex) {
				error_log('ERROR: SQL query failed');
				error_log($ex->getMessage());
				database_cleanup(false);
				die();
			}
		}
		$node_ids = array();
	}
	$number_of_saved_addresses = $db->query('select count(*) from `osmdata`')->fetchColumn();
	$db = NULL;
	if(!($number_of_addresses - $number_of_errors) === $number_of_saved_addresses) {
		error_log('ERROR: While saving OSM data. Number of downloaded addresses does not match number of saved addresses. Aborting.');
		error_log('Downloaded addresses/addresses with errors/saved addresses: ' . $number_of_addresses . '/' . $number_of_errors . '/' . $number_of_saved_addresses);
		database_cleanup();
		die();
	}
	error_log('INFO: ' . $number_of_saved_addresses . ' addresses downloaded from OSM');
	return $number_of_saved_addresses;
}

function download_aws_data(): int {
	//via AWS API, downloads address data for the given postcode
	//inserts the returned JSON data into database table 'awsdata'
	//closes the database connection and returns the number of rows in 'awsdata'
	$string = file_get_contents('https://api.dataforsyningen.dk/adgangsadresser?postnr=' . POSTNUMMER . '&struktur=mini&status=1'); // status 1 = gældende (ej foreløbig, henlagt, nedlagt)
	if($string === FALSE) {
		error_log('ERROR: Unable to contact DAWA API (file_get_contents returned false)');
		database_cleanup(false);
		die();
	}
	$json = json_decode($string, true);
	if(!is_array($json)) {
		error_log('ERROR: json_decode returned ' . gettype($json) . ' (should be array) in download_aws_data()');
		database_cleanup(false);
		die();
	}
	$number_of_addresses = count($json);
	$number_of_errors = 0;
	$street_name_fixes = street_name_fixes();
	$db = db_connect();
	$stmt = $db->prepare('INSERT INTO `awsdata` (`osak:identifier`, `lat`, `lon`, `postnummer_navn`, `husnr`, `postnummer_nr`, `vejstykke_navn`, `kommune_navn`, `sup_bynavn`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
	$kommuner = array();
	foreach($json as $addr){
		if(($number_of_addresses - $number_of_errors) / $number_of_addresses < 0.95) {
			error_log('ERROR: While processing AWS data, more than 5% of downloaded addresses caused errors. Aborting.');
			database_cleanup();
			die();
		}
		
		//Address ID. Checked to follow UUID format
		if(!preg_match('/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/', $addr['id'])) {
			error_log('ERROR: While processing AWS data.');
			error_log('	Downloaded address ID was: (' . gettype($addr['id']) . ') ' . $addr['id']);
			error_log('	Skipping this address');
			++$number_of_errors;
			continue;
		}
		$a = (string) strtoupper(str_replace('-','',$addr['id']));

		//Address Y coordinate
		try {
			$b = (float) number_format($addr['y'], 6, '.', ''); //the old script used to update addresses only worked with 6 decimals of precision in coordinates, for unknown reasons. This limit is retained since otherwise it becomes difficult to compare address locations
			if($b < 54 || $b > 58) {
				throw new Exception('Y coordinate out of range (' . $b . ')');
			}
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (address Y coordinate)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//Address X coordinate
		try {
			$c = (float) number_format($addr['x'], 6, '.', '');
			if($c < 7 || $c > 16) {
				throw new Exception('X coordinate out of range (' . $c . ')');
			}
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (address X coordinate)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//City name
		try {
			$d = (string) $addr['postnrnavn'];
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (address city name/postnrnavn)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//Street number
		try {
			$e = (string) $addr['husnr']; //house numbers can contain letters (nnA, nnB, nnC, ...)
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (street number)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//Postcode
		try {
			$f = (int) $addr['postnr'];
			if($f !== POSTNUMMER) {
				throw new Exception('The address postcode (' . $f . ') does not match the postcode currently being updated');
			}
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (address postcode)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//Street name
		try {
			if(is_null($addr['vejkode']) || is_null($addr['kommunekode'])) {
				$g = (string) $addr['vejnavn'];
				error_log('WARNING: AWS Vejkode or Kommunekode is NULL. Using raw street name from AWS (instead of applying street name fixes)');
			}
			else {
				$g = (string) street_name($street_name_fixes, $addr['vejnavn'], $addr['vejkode'], $addr['kommunekode']);
			}
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (address street name)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//Municipality
		try {
			if(!array_key_exists((string) $addr['kommunekode'], $kommuner)) {
				$kommuner[$addr['kommunekode']] = kommune_navn($addr['kommunekode']); //when downloading AWS data with struktur=mini, only the kommunekode is included, not the kommunenavn
			}
			$h = $kommuner[$addr['kommunekode']];
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (municipality name)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		//Place name
		try {
			$i = $addr['supplerendebynavn'];
			if($i == $d) { //in some places in DAR, the supplerende bynavn is set even though it is equal to the postnummer navn. Let's remove that.
				$i = NULL;
			}
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (place name/supplerende bynavn)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}

		try {
			$stmt->execute([$a,$b,$c,$d,$e,$f,$g,$h,$i]);
		}
		catch(Exception $ex) {
			error_log('ERROR: While processing AWS data (saving address to database)');
			error_log($ex->getMessage());
			error_log('	Skipping address with ID ' . $a);
			++$number_of_errors;
			continue;
		}
	}

	//check that we now have the expected number of addresses in the database
	$number_of_saved_addresses = $db->query('select count(*) from `awsdata`')->fetchColumn();
	if(!($number_of_addresses - $number_of_errors) === $number_of_saved_addresses) {
		error_log('ERROR: While saving AWS data. Number of downloaded addresses does not match number of saved addresses. Aborting.');
		error_log('Downloaded addresses/addresses with errors/saved addresses: ' . $number_of_addresses . '/' . $number_of_errors . '/' . $number_of_saved_addresses);
		database_cleanup();
		die();
	}
	$db = NULL;
	error_log('INFO: ' . $number_of_saved_addresses . ' addresses downloaded from AWS');
	return $number_of_saved_addresses;
}

function kommune_navn($kode): string {
	$string = @file_get_contents('https://api.dataforsyningen.dk/kommuner?kode=' . $kode);
	if($string === FALSE) {
		error_log('ERROR: Unable to contact AWS (file_get_contents returned false in kommune_navn())');
		database_cleanup(false);
		die();
	}
	return json_decode($string, true)[0]['navn'];
}

function download_ois_fixes(): int {
	//downloads a list of street name fixes from https://oisfixes.iola.dk and loads them into the ois_fixes database table
	$db = db_connect();
	$stmt = $db->prepare('INSERT INTO `ois_fixes`(`fixed_name`, `mun_no`, `str_no`) VALUES (?, ?, ?)');
	$string = file_get_contents('https://oisfixes.iola.dk/api/getwaycorrections/');
	if($string === FALSE) {
		error_log('ERROR: Unable to contact OISFIXES (file_get_contents returned false in download_ois_fixes())');
		database_cleanup(false);
		die();
	}
	$results = json_decode($string);
	$datas = $results->data;
	if(count($datas) > 0) {
		$db->exec('TRUNCATE TABLE `ois_fixes`;');
	}
	else {
		error_log('WARNING: Unable to download street name fixes');
		return 0;
	}
	foreach($datas as $data) {
		$fixed_name = $data[1];
		$mun_no = $data[2];
		$str_no = $data[3];
		try {
			$stmt->execute([$fixed_name, $mun_no, $str_no]);
		}
		catch(PDOException $ex) {
			error_log('WARNING: Failed saving street name fix to database');
			error_log($ex->getMessage());
			continue;
		}
	}
	$antal = $db->query('select count(*) from `ois_fixes`')->fetchColumn();
	$db = NULL;
	error_log('INFO: ' . $antal . ' fixed street names downloaded from oisfixes.iola.dk');
	return $antal;
}

function create_changeset(): int {
	//calls the OSM API to open a new changeset
	//returns the new changeset ID
	$changeset = new SimpleXMLElement('<osm><changeset></changeset></osm>');
	add_tag($changeset, "created_by=autoAWS " . VERSION . " (" . VERSION_COMMIT_SHORT . ")");
	add_tag($changeset, "comment=Adresser opdateret for postnummer " . POSTNUMMER);
    add_tag($changeset, "source=Danmarks Adresseregister");
	add_tag($changeset, "bot=yes");
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_POSTFIELDS => html_entity_decode($changeset->asXML()),
		CURLOPT_URL => OSM_API_URL.'changeset/create')
	);
	if(DEBUG) { //the CHANGESET const should still be defined if we are in debug mode, since many functions rely on it
		return 1234;
	}

	$ch_res = curl_exec($ch);
	$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
	if($ch_res === false) {
		error_log('ERROR: Failed to create changeset.');
		error_log('	cURL error number: ' . curl_errno($ch));
		error_log('	cURL error message: ' . curl_error($ch));
		database_cleanup(false);
		die();
	}
	switch($resp) {
		case 200:
			$changeset_id = (int) $ch_res;
			break;
		default:
			error_log('ERROR: Failed to create changeset. OSM API returned HTTP status ' . $resp);
			database_cleanup(false);
			die();
			break;
	}
	curl_close($ch);
	error_log('INFO: New changeset created with ID: ' . $changeset_id);
	return $changeset_id;
}

function close_changeset(int $chg = CHANGESET): void {
	error_log('INFO: Closing changeset');
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_URL => OSM_API_URL.'changeset/'.$chg.'/close')
	);
	if(!DEBUG) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		switch($resp) {
			case 200: //all is fine
				break;
			default:
				error_log('WARNING: Failed to close changeset with ID ' . $chg . '. OSM API returned HTTP status ' . $resp);
				break;
		}
	}
	curl_close($ch);
}

function detect_add(): void {
	//inserts addresses in `add_addr` that exist in the `awsdata` table but not in the `osmdata` table (compared on the osak:identifier value)
	$db = db_connect();
	$sql = '
		INSERT INTO     `add_addr`(`osak:identifier`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT          `awsdata`.`osak:identifier`, `awsdata`.`lat`, `awsdata`.`lon`, `awsdata`.`postnummer_navn`, "DK", `awsdata`.`husnr`, `awsdata`.`postnummer_nr`, `awsdata`.`vejstykke_navn`, `awsdata`.`kommune_navn`, `awsdata`.`sup_bynavn`
		FROM            `awsdata`
		WHERE           `awsdata`.`osak:identifier` NOT IN(
			SELECT      `osmdata`.`osak:identifier`
			FROM        `osmdata`
			WHERE       1);
		';
	$db->exec($sql);
	$db = NULL;
}

function detect_updates(): void {
	//compares addresses in `awsdata` and `osmdata` and adds addresses to `update_addr` if an update is needed
	$db = db_connect();
	$sql = '
		INSERT INTO    `update_addr`(`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT         `awsdata`.`osak:identifier`, `osmdata`.`node_id`, `awsdata`.`lat`, `awsdata`.`lon`, `awsdata`.`postnummer_navn`, "DK", `awsdata`.`husnr`, `awsdata`.`postnummer_nr`, `awsdata`.`vejstykke_navn`, `awsdata`.`kommune_navn`, `awsdata`.`sup_bynavn`
		FROM           `awsdata`
		INNER JOIN     `osmdata` ON `awsdata`.`osak:identifier` = `osmdata`.`osak:identifier`
		WHERE         (`osmdata`.`lat` <> `awsdata`.`lat`
			OR         `osmdata`.`lon` <> `awsdata`.`lon`
			OR         `osmdata`.`addr:city` <> `awsdata`.`postnummer_navn`
			OR		   `osmdata`.`addr:city` IS NULL
			OR         `osmdata`.`addr:housenumber` <> `awsdata`.`husnr`
			OR         `osmdata`.`addr:housenumber` IS NULL
			OR         `osmdata`.`addr:street` <> `awsdata`.`vejstykke_navn`
			OR         `osmdata`.`addr:street` IS NULL
			OR         `osmdata`.`addr:municipality` <> `awsdata`.`kommune_navn`
			OR		   `osmdata`.`addr:municipality` IS NULL
			OR		   `osmdata`.`addr:place` <> `awsdata`.`sup_bynavn`
			OR		   (`osmdata`.`addr:place` IS NOT NULL AND `awsdata`.`sup_bynavn` IS NULL)
			OR		   (`osmdata`.`addr:place` IS NULL AND `awsdata`.`sup_bynavn` IS NOT NULL)
					   )
		AND            `osmdata`.`autoaws_ignore` = 0;
		';
	//forces an update of all addresses, useful if the tagging structure has been changed
	if(UPDATE_ALL) {
		$sql = '
		INSERT INTO    `update_addr`(`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT         `awsdata`.`osak:identifier`, `osmdata`.`node_id`, `awsdata`.`lat`, `awsdata`.`lon`, `awsdata`.`postnummer_navn`, "DK", `awsdata`.`husnr`, `awsdata`.`postnummer_nr`, `awsdata`.`vejstykke_navn`, `awsdata`.`kommune_navn`, `awsdata`.`sup_bynavn`
		FROM           `awsdata`
		INNER JOIN     `osmdata` ON `awsdata`.`osak:identifier` = `osmdata`.`osak:identifier`
		WHERE          `osmdata`.`autoaws_ignore` = 0;
		';
	}
	$db->exec($sql);
	$db = NULL;
}

function detect_delete(): void {
	//populate delete_addr table
	$db = db_connect();
	
	//find addresses present in OSM data but not in AWS data
	$sql = '
		INSERT INTO     `delete_addr`(`node_id`, `lat`, `lon`)
		SELECT          `osmdata`.`node_id`, `osmdata`.`lat`, `osmdata`.`lon`
		FROM            `osmdata`
		WHERE           `osmdata`.`osak:identifier` NOT IN(
			SELECT      `awsdata`.`osak:identifier`
			FROM        `awsdata`
			WHERE       1)
		AND            `osmdata`.`autoaws_ignore` = 0;
		';
	try {
		$db->exec($sql);
	}
	catch(PDOException $ex) {
		error_log('ERROR: detect_delete() SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	
	//copy addresses from invalid_node table
	$sql = '
		INSERT INTO     `delete_addr`(`node_id`, `lat`, `lon`)
		SELECT          `invalid_node`.`node_id`, `invalid_node`.`lat`, `invalid_node`.`lon`
		FROM            `invalid_node`
		WHERE       1;
		';
	try {
		$db->exec($sql);
	}
	catch(PDOException $ex) {
		error_log('ERROR: detect_delete() SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	
	$db = NULL;
	detect_delete_add();
}

function detect_delete_add(): void {
	//finds cases where an address node is going to be deleted and a new node created again at the same position
	//removes such addresses from the `add_addr` and `delete_addr` tables and inserts them into `update_addr` instead
	//this means that the existing node will be updated instead of being deleted and re-added
	$db = db_connect();
	try {
		$db->exec('
		INSERT INTO			`update_addr`(`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT				`add_addr`.`osak:identifier`, `delete_addr`.`node_id`, `add_addr`.`lat`, `add_addr`.`lon`, `add_addr`.`addr:city`, `add_addr`.`addr:country`, `add_addr`.`addr:housenumber`, `add_addr`.`addr:postcode`, `add_addr`.`addr:street`, `add_addr`.`addr:municipality`, `add_addr`.`addr:place`
		FROM				`add_addr`
		INNER JOIN			`delete_addr` ON `add_addr`.`lat` = `delete_addr`.`lat` AND `add_addr`.`lon` = `delete_addr`.`lon`
		');
	}
	catch (Exception $ex) {
		error_log('ERROR: detect_delete_add() SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}

	//if an address now exists in update_addr, remove it from add_addr and/or delete_addr
	$db->exec('
	DELETE FROM `add_addr`
	WHERE `add_addr`.`osak:identifier` IN(
		SELECT `update_addr`.`osak:identifier`
		FROM `update_addr`
		WHERE 1);
	');
	$db->exec('
	DELETE FROM `delete_addr`
	WHERE `delete_addr`.`node_id` IN(
		SELECT `update_addr`.`node_id`
		FROM `update_addr`
		WHERE 1);
	');

	//import the xml node string into the update and delete tables
	try {
		$db->exec('
		UPDATE `update_addr`
		JOIN `xmlnodes` ON `update_addr`.`node_id` = `xmlnodes`.`node_id`
		SET `update_addr`.`xml` = `xmlnodes`.`xml`
		WHERE `update_addr`.`node_id` = `xmlnodes`.`node_id`
		');
	}
	catch(Exception $ex) {
		error_log('ERROR: Failed to add XML node string to update_addr database table');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	try {
		$db->exec('
		UPDATE `delete_addr`
		JOIN `xmlnodes` ON `delete_addr`.`node_id` = `xmlnodes`.`node_id`
		SET `delete_addr`.`xml` = `xmlnodes`.`xml`
		WHERE `delete_addr`.`node_id` = `xmlnodes`.`node_id`
		');
	}
	catch(Exception $ex) {
		error_log('ERROR: Failed to add XML node string to delete_addr database table');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	$db = NULL;
}

function count_updates(): array {
	//returns the number of records in the update, add and delete tables as well as their sum
	$db = db_connect();
	$update = (int) $db->query('select count(*) from `update_addr`')->fetchColumn();
	error_log('INFO: ' . $update . ' addresses need updating');
	$add = (int) $db->query('select count(*) from `add_addr`')->fetchColumn();
	error_log('INFO: ' . $add . ' addresses are missing from OSM and will be added');
	$delete = (int) $db->query('select count(*) from `delete_addr`')->fetchColumn();
	error_log('INFO: ' . $delete . ' addresses will be deleted from OSM');
	$sum = (int) $update+$add+$delete;
	$db = NULL;
	return array($sum, $update, $add, $delete);
}

function delete_osm_node(int $node, float $lat, float $lon): void {
	//inserts a given node ID into the invalid_node table
	//this node will be added to delete_addr later by detect_delete
	$db = db_connect();
	$stmt = $db->prepare('INSERT INTO `invalid_node`(`node_id`, `lat`, `lon`) VALUES (?, ?, ?)');
	try {
		$stmt->execute([$node, $lat, $lon]);
	}
	catch(PDOException $ex) {
		error_log('WARNING: SQL exception in delete_osm_node()');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	$db = NULL;
}

function split_changeset(int $sum): void {
	//an OSM changeset cannot contain more than 10.000 edits.
	//if we have more than 9.000 changes, randomly discard 50% of the changes
	//if we have more than 19.000 changes, randomly discard 67% of the changes
	//if we have more than 29.000 changes, randomly discard 75% of the changes
	//if we have more than 39.000 changes, randomly discard 80% of the changes
	//if CHANCE is not defined as 100 (meaning that not all edits for the given postcode are submitted in the current changeset),
	//the same postcode will be selected again next time the script is run.
	if($sum > 39000) {
		error_log('INFO: Many changes needed. Will be split into multiple changesets.');
		define('CHANCE', 20);
	}
	elseif($sum > 29000) {
		error_log('INFO: Many changes needed. Will be split into multiple changesets.');
		define('CHANCE', 25);
	}
	elseif($sum > 19000) {
		error_log('INFO: Many changes needed. Will be split into multiple changesets.');
		define('CHANCE', 33);
	}
	elseif($sum > 9000) {
		error_log('INFO: Many changes needed. Will be split into multiple changesets.');
		define('CHANCE', 50);
	}
	else {
		define('CHANCE', 100);
	}
}

function get_addr_tags(): array {
	//return an array of all recognised address tags, i.e. tags we are comfortable deleting in case we need to remove an address
	//NOTE!!! if updating this list, also update the $xpath_string variable in delete_nodes()
	return array(
		"addr:street",
		"addr:housenumber",
		"osak:house_no",
		"osak:street_name",
		"osak:identifier",
		"addr:city",
		"addr:country",
		"addr:housenumber",
		"addr:postcode",
		"osak:municipality_name",
		"addr:municipality",
		"osak:revision",
		"osak:municipality_no",
		"osak:street_no",
		"source",
		"osak:street",
		"osak:subdivision",
		"addr:place",
		"ois:fixme",
		"fixme"
	);
}

## Functions used to work with data from the database and submit it to the OSM API ##

function create_nodes(): void {
	error_log('INFO: Creating nodes');
	//submits to the OSM API new addresses as imported from add_addr
	$db = db_connect();
	$stmt = $db->prepare('SELECT * FROM `add_addr`');
	try {
		$stmt->execute();
		$results = $stmt->fetchAll();
	}
	catch(Exception $ex) {
		error_log('ERROR: SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_URL => OSM_API_URL.'node/create')
	);	
	foreach($results as $result) { //OSM API only allows for creating one node at the time, so we have to call the API once for every single node
		$node = new SimpleXMLElement('<osm><node></node></osm>');
		$node->children()[0]->addAttribute('changeset', CHANGESET);
		$node->children()[0]->addAttribute('lat', $result['lat']);
		$node->children()[0]->addAttribute('lon', $result['lon']);
		$osak_id = id_format($result['osak:identifier']);
		$city = $result['addr:city'];
		$country = $result['addr:country'];
		$house_no = $result['addr:housenumber'];
		$postcode = $result['addr:postcode'];
		$street = $result['addr:street'];
		$municipality_name = $result['addr:municipality'];
		$place = $result['addr:place'];
		add_tag($node, "source=Danmarks Adresseregister");
		add_tag($node, "osak:identifier={$osak_id}");
		add_tag($node, "addr:city={$city}");
		add_tag($node, "addr:country={$country}");
		add_tag($node, "addr:housenumber={$house_no}");
		add_tag($node, "addr:postcode={$postcode}");
		add_tag($node, "addr:street={$street}");
		add_tag($node, "addr:municipality={$municipality_name}");
		if($place !== NULL) {
			add_tag($node, "addr:place={$place}");
		}
		$xml = html_entity_decode($node->asXML());
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		if(!DEBUG AND rand(0,100) <= CHANCE) {
			curl_exec($ch);
			$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
			switch($resp) {
				case 200: //all is fine
					break;
				default:
					error_log('WARNING: Failed to create node for address ID ' . $osak_id . '. OSM API returned HTTP status ' . $resp);
					break;
			}
		}
	}
	$db = NULL;
}

function detect_parent_ways($node_id): bool {
	//returns TRUE if a given node is part of a way, or FALSE if the given node is standalone (not part of a way)
	$parent_ways = new SimpleXMLElement(file_get_contents('https://api.openstreetmap.org/api/0.6/node/' . $node_id . '/ways'));
	if($parent_ways->children()[0] == NULL) {
		return FALSE;
	}
	return TRUE;
}

function update_node_in_way($result): bool {
	//if a node is a member of a way, we do not want to update the node (since moving the node might deform the way)
	//instead, remove all address tags from the node in question, and create a new standalone node with the updated address values
	
	//create a new node with the address
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_URL => OSM_API_URL.'node/create')
	);
	$node = new SimpleXMLElement('<osm><node></node></osm>');
	$node->children()[0]->addAttribute('changeset', CHANGESET);
	$node->children()[0]->addAttribute('lat', $result['lat']);
	$node->children()[0]->addAttribute('lon', $result['lon']);
	$osak_id = id_format($result['osak:identifier']);
	$city = $result['addr:city'];
	$country = $result['addr:country'];
	$house_no = $result['addr:housenumber'];
	$postcode = $result['addr:postcode'];
	$street = $result['addr:street'];
	$municipality_name = $result['addr:municipality'];
	$place = $result['addr:place'];
	add_tag($node, "source=Danmarks Adresseregister");
	add_tag($node, "osak:identifier={$osak_id}");
	add_tag($node, "addr:city={$city}");
	add_tag($node, "addr:country={$country}");
	add_tag($node, "addr:housenumber={$house_no}");
	add_tag($node, "addr:postcode={$postcode}");
	add_tag($node, "addr:street={$street}");
	add_tag($node, "addr:municipality={$municipality_name}");
	if($place !== NULL) {
		add_tag($node, "addr:place={$place}");
	}
	$xml = html_entity_decode($node->asXML());
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	if(!DEBUG) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		switch($resp) {
			case 200: //all is fine
				break;
			default:
				error_log('WARNING: Failed to create node for address ID ' . $osak_id . '. OSM API returned HTTP status ' . $resp);
				break;
		}
	}
	unset($ch, $node);
	$node = new SimpleXMLElement('<osm>'.$result['xml'].'</osm>');
	
	//remove all address tags from the existing node (which is part of a way)
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST => "PUT"
		));
	if(!$node) {
		return FALSE;
	}
	$node->node[0]['changeset'] = CHANGESET;
	$node_string = (string) $node->asXML();
	$valid_tags = get_addr_tags();
	foreach($valid_tags as $tag) {
		$node_string = preg_replace('/<tag k="' . $tag . '" v=".+"\/>/', '', $node_string);
	}
	curl_setopt($ch, CURLOPT_URL, OSM_API_URL.'node/'.$result['node_id']);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $node_string);
	if(!DEBUG) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		switch($resp) {
			case 200: //all is fine
				break;
			default:
				error_log('WARNING: Failed to remove address tags from node with ID ' . $result['node_id'] . '. OSM API returned HTTP status ' . $resp);
				break;
		}
	}
	return TRUE;
}

function delete_node_in_way($node, $id): bool {
	//remove all address tags from a node which is part of a way (delete the address without actually deleting the node)
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST => "PUT"
		));
	$node->node[0]['changeset'] = CHANGESET;
	$node_string = (string) $node->asXML();
	$valid_tags = get_addr_tags();
	foreach($valid_tags as $tag) {
		$node_string = preg_replace('/<tag k="' . $tag . '" v=".+"\/>/', '', $node_string);
	}
	curl_setopt($ch, CURLOPT_URL, OSM_API_URL.'node/'.$id);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $node_string);
	if(!DEBUG) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		switch($resp) {
			case 200: //all is fine
				break;
			default:
				error_log('WARNING: Failed to remove address tags from node with ID ' . $id . '. OSM API returned HTTP status ' . $resp);
				break;
		}
	}
	return TRUE;
}

function update_nodes(): void {
	error_log('INFO: Updating nodes');
	$db = db_connect();
	$stmt = $db->prepare('SELECT * FROM `update_addr`');
	try {
		$stmt->execute();
		$results = $stmt->fetchAll();
	}
	catch(Exception $ex) {
		error_log('ERROR: SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	$db = NULL;
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST => "PUT"
		));
	$error = array();
	foreach($results as $result) {
		$node = new SimpleXMLElement('<osm>'.$result['xml'].'</osm>');
		if(!$node) {
			continue; //not able to get node, skip to the next one
		}
		if(detect_parent_ways($result['node_id'])) {
			update_node_in_way($result);
			continue;
		}
		$node->node[0]['lat'] = $result['lat'];
		$node->node[0]['lon'] = $result['lon'];
		$node->node[0]['changeset'] = CHANGESET;
		
		$node_string = (string) $node->asXML();
		//it's easier to do simple regex replace when updating tag values instead of having to search through <tag> XML elements
		$missing_tags = array(
		'addr:city'=>FALSE,
		'addr:housenumber'=>FALSE,
		'addr:street'=>FALSE,
		'addr:municipality'=>FALSE,
		'addr:postcode'=>FALSE,
		'addr:place'=>FALSE,
		'addr:country'=>FALSE
		);
		$node_string = preg_replace('/<tag k="osak:identifier" v=".+"\/>/', '<tag k="osak:identifier" v="'.id_format($result['osak:identifier']).'"/>', $node_string);
		$node_string = preg_replace('/<tag k="addr:city" v=".+"\/>/', '<tag k="addr:city" v="'.$result['addr:city'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:city'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:housenumber" v=".+"\/>/', '<tag k="addr:housenumber" v="'.$result['addr:housenumber'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:housenumber'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:street" v=".+"\/>/', '<tag k="addr:street" v="'.$result['addr:street'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:street'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:municipality" v=".+"\/>/', '<tag k="addr:municipality" v="'.$result['addr:municipality'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:municipality'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:postcode" v=".+"\/>/', '<tag k="addr:postcode" v="'.$result['addr:postcode'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:postcode'] = TRUE;
		}
		if($result['addr:place'] !== NULL ){
			$node_string = preg_replace('/<tag k="addr:place" v=".+"\/>/', '<tag k="addr:place" v="'.$result['addr:place'].'"/>', $node_string, -1, $count);
			if($count == 0) {
				$missing_tags['addr:place'] = TRUE;
			}
		}
		if($result['addr:place'] == NULL ){
			$node_string = preg_replace('/<tag k="addr:place" v=".+"\/>/', '', $node_string);
		}
		$node_string = preg_replace('/<tag k="addr:country" v=".+"\/>/', '<tag k="addr:country" v="DK"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:country'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="osak:subdivision" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:house_no" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:municipality_no" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:municipality_name" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:street_name" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:street_no" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:street" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:revision" v=".+"\/>/', '', $node_string);
		
		//the following if is run if one of the seven tags defined in $missing_tags is not found on the given node
		//the node (currently represented as an XML string) is then converted to an XML-object, the missing tag is added, and it is converted back to an xml-string
		if(array_search(TRUE, $missing_tags) != FALSE) {
			$node = new SimpleXMLElement($node_string);
			foreach($missing_tags AS $tag => $missing) {
				if($missing) {
					add_tag($node, $tag . '=' . $result[$tag]);
				}
			}
			$node_string = (string) $node->asXML();
		}
		
		curl_setopt($ch, CURLOPT_URL, OSM_API_URL.'node/'.$result['node_id']);
		$xml = html_entity_decode($node_string);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $node_string);
		if(!DEBUG AND rand(0,100) <= CHANCE) {
			curl_exec($ch);
			$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
			switch($resp) {
				case 200: //all is fine
					break;
				default:
					error_log('WARNING: Failed to update node with ID ' . $result['node_id'] . '. OSM API returned HTTP status ' . $resp);
					break;
			}
		}
	}
	if(!empty($error)) {
		error_log('WARNING: The following address(es) may not have been updated correctly');
		foreach($error AS $err) {
			error_log($err);
		}
	}
}

function update_node(string $xml_node, int $id): void {
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST => "PUT",
		CURLOPT_URL => OSM_API_URL.'node/'.$id,
		CURLOPT_POSTFIELDS => $xml_node
		));
	if(!DEBUG) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		switch($resp) {
			case 200: //all is fine
				break;
			default:
				error_log('WARNING: Failed to update node with ID ' . $id . '. OSM API returned HTTP status ' . $resp);
				break;
		}
	}
}

function delete_nodes(): void {
	error_log('INFO: Deleting nodes');
	//these are the tags we are happy with deleting. If any OTHER tags exist, we want to preserve them
	$xpath_string = 'node/tag[not(
		@k="addr:street"
		or @k="addr:housenumber"
		or @k="osak:house_no"
		or @k="osak:street_name"
		or @k="osak:identifier"
		or @k="addr:city"
		or @k="addr:country"
		or @k="addr:housenumber"
		or @k="addr:postcode"
		or @k="osak:municipality_name"
		or @k="addr:municipality"
		or @k="osak:revision"
		or @k="osak:municipality_no"
		or @k="osak:street_no"
		or @k="source"
		or @k="osak:street"
		or @k="osak:subdivision"
		or @k="addr:place"
		or @k="ois:fixme"
		or @k="fixme"
	)]';
	$valid_tags = get_addr_tags();
	$db = db_connect();
	$stmt = $db->prepare('SELECT * FROM `delete_addr`');
	try {
		$stmt->execute();
		$results = $stmt->fetchAll();
	}
	catch(Exception $ex) {
		error_log('ERROR: SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	foreach($results as $result) { //iterate over all nodes with addresses that are to be deleted
		$id = (int) $result['node_id'];
		$node = new SimpleXMLElement('<osm>'.$result['xml'].'</osm>');
		if(!$node) {
			error_log('WARNING: Failed to delete node with ID ' . $id);
			continue; //not able to get node, skip to the next one
		}
		//in case an address has been moved from one postcode to another (position remains constant but postcode changes),
		//we will delete the address node, but then make sure the new postcode is updated ASAP so the address can be added again
		update_postcode_deleted_node($result['lat'], $result['lon']);
		$xpath = $node->xpath($xpath_string); //check if the node has tags that should be preserved
		if(!empty($xpath)) { //node has additional tags that should be preserved
			$tags = count($node->children()[0]->children());
			for($i = $tags; $i >= 0; --$i) {
				if(in_array(strval($node->children()[0]->tag[$i]['k']), $valid_tags)) { //remove all address nodes, leaving only the extra nodes
					$dom = dom_import_simplexml($node->children()[0]->tag[$i]);
					$dom->parentNode->removeChild($dom);
				}
			}
			//at this point, all address tags are deleted
			$node->children()[0]['changeset'] = CHANGESET;
			add_tag($node, 'fixme=This address no longer exists. Please check if the tags on this node are still valid');
			update_node(html_entity_decode($node->asXML()), $id);
		}
		else { //no additional tags, just delete the node
			$node->children()[0]['changeset'] = CHANGESET;
			delete_node($node->asXML(), $id);
		}
	}
	$db = NULL;
}

function delete_node(string $xml_node, int $id): void {
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BEARER,
		CURLOPT_XOAUTH2_BEARER => OSM_API_OAUTH2_TOKEN,
		CURLOPT_CUSTOMREQUEST => "DELETE",
		CURLOPT_URL => OSM_API_URL.'node/'.$id,
		CURLOPT_POSTFIELDS => $xml_node
		));
	if(!DEBUG AND rand(0,100) <= CHANCE) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
		switch($resp) {
			case 200: //all is fine
				break;
			case 412: //node is part of a way or relation
				$node = new SimpleXMLElement($xml_node);
				delete_node_in_way($node, $id);
				break;
			default:
				error_log('WARNING: Tried deleting node with ID ' . $id . ' but OSM API returned HTTP status ' . $resp);
				break;
		}
	}
}

function update_postcode_deleted_node(float $lat, float $lon): void {
	//given a lat,lon pair, this function checks if an address exists in that position in DAR and, if so, if that address is in another postcode than the one being updated currently
	//if so, that other postcode will be updated next. This is to ensure that if an address node is deleted as a result of its postcode changing, it will be added again quickly with the new postcode
	$addr = json_decode(file_get_contents('https://api.dataforsyningen.dk/adgangsadresser?cirkel='.$lon.','.$lat.',1&struktur=mini'));
	if(!empty($addr)) {
		$postcode = (int) $addr[0]->postnr;
		if($postcode != POSTNUMMER AND !DEBUG) {
			error_log('INFO: While updating ' . POSTNUMMER . ', a change seems to affect ' . $postcode . ', which will be updated next');
			$db = db_connect();
			$db->exec('UPDATE `last_update` SET `last_update`.`opdateret` = "2017-01-01 00:00:01" WHERE `last_update`.`postnummer` = ' . $postcode);
			$db = NULL;
		}
	}
}

## Misc. functions ##

function opdater_postnumre(): void {
	error_log('INFO: Updating the list of postcodes');
	$db = db_connect();
	$stmt = $db->prepare('SELECT `postnummer` FROM `last_update`');
	try {
		$stmt->execute();
		$db_postnumre = $stmt->fetchAll(PDO::FETCH_COLUMN);
	}
	catch(Exception $ex) {
		error_log('ERROR: SQL query failed');
		error_log($ex->getMessage());
		database_cleanup(false);
		die();
	}
	$aws_data = json_decode(file_get_contents('https://api.dataforsyningen.dk/postnumre/'));
	$aws_postnumre = array();
	foreach($aws_data as $nummer) {
		array_push($aws_postnumre, intval($nummer->nr));
	}
	if(count($aws_postnumre) < 700) { //we expect around 1000 postcodes at least
		error_log('ERROR: Error reading list of postcodes. The list was unexpectedly short (' . count($aws_postnumre) . ' postcodes)');
		database_cleanup(false);
		die();
	}
	$delete = array_diff($db_postnumre, $aws_postnumre);
	$add = array_diff($aws_postnumre, $db_postnumre);
	//delete non-existing postcodes
	foreach($delete as $del) {
		$db->exec('UPDATE `last_update` SET `last_update`.`deleted` = 1 WHERE `last_update`.`postnummer` = '. $del); //when a postcode is discontinued, the deleted value is set to 1. Postcodes with a deleted value of 1 will be picked first for the next update. After being updated once, postcodes with a deleted value of 1 will be removed from the database.
	}
	//add missing postcodes
	$stmt = $db->prepare('INSERT INTO `last_update`(`postnummer`, `opdateret`) VALUES (?,?)');
	foreach($add as $a) {
		try {
			$stmt->execute([$a, '2017-01-01 00:00:00']); //sets a date in the past to force the script to update the adresses in this postcode as soon as possible
		}
		catch(Exception $ex) {
			error_log('ERROR: SQL query failed');
			error_log($ex->getMessage());
			database_cleanup(false);
			die();
		}
	}
	$db = NULL;
}

function select_postcode(): int {
	//select and define the next postcode to update
	if(MANUAL) {
		$code = MANUAL_CODE;
	}
	else {
		$db = db_connect();
		$stmt = $db->prepare('SELECT `postnummer`, `deleted` FROM `last_update` ORDER BY `deleted` DESC, `opdateret` ASC, `postnummer` ASC LIMIT 1');
		try {
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				$fetch = $stmt->fetch();
				$postnr = (int) $fetch['postnummer'];
				if($postnr >= 1000 AND $postnr <= 9999) {
					$code = $postnr;
				}
				else {
					throw new Exception();
				}
				if($fetch['deleted'] == 1) {
					$db->exec('DELETE FROM `last_update` WHERE `last_update`.`postnummer` = '.POSTNUMMER);
				}
			}
			else {
				throw new Exception();
			}
		}
		catch(Exception $ex) {
			error_log('ERROR: Unable to determine next postcode to update. The attempted postcodes was ' . $postnr);
			opdater_postnumre();
			die();
		}
		$db = NULL;
	}
	error_log('INFO: Now updating postcode ' . $code);
	return $code;
}

function database_cleanup($finish_postcode = true): void {
	error_log('INFO: Clearing database');
	$db = db_connect();
	if(!defined('CHANCE')) {
		define('CHANCE', 100);
	}
	if(CHANCE == 100 AND !DEBUG AND $finish_postcode AND defined('POSTNUMMER')) {
		$db->exec('UPDATE `last_update` SET `last_update`.`opdateret` = "' . date("Y-m-d H:i:s") . '" WHERE `last_update`.`postnummer` = ' . POSTNUMMER);
	}
	$db->exec('TRUNCATE TABLE `awsdata`;');
	$db->exec('TRUNCATE TABLE `add_addr`;');
	$db->exec('TRUNCATE TABLE `delete_addr`;');
	$db->exec('TRUNCATE TABLE `update_addr`;');
	$db->exec('TRUNCATE TABLE `invalid_node`;');
	$db->exec('TRUNCATE TABLE `xmlnodes`;');
	$db->exec('TRUNCATE TABLE `osmdata`;');
	$db->exec('TRUNCATE TABLE `running_lock`');
	$db = NULL;
	error_log('INFO: Database cleared');
}
