<?php

require_once 'config.php';

database_cleanup();

function db_connect(): PDO {
	//connects to database using PDO and returns a PDO object
	$charset = 'utf8mb4';
	$dsn = "mysql:host=" . MYSQL_HOST . ";dbname=" . MYSQL_DB . ";charset=$charset";
	$opt = [
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		PDO::ATTR_EMULATE_PREPARES   => false,
	];
	return new PDO($dsn, MYSQL_USERNAME, MYSQL_PASSWORD, $opt);
}

function database_cleanup() {
	$db = db_connect();
	$db->exec('TRUNCATE TABLE `osmdata`;');
	$db->exec('TRUNCATE TABLE `awsdata`;');
	$db->exec('TRUNCATE TABLE `add_addr`;');
	$db->exec('TRUNCATE TABLE `delete_addr`;');
	$db->exec('TRUNCATE TABLE `update_addr`;');
	$db->exec('TRUNCATE TABLE `invalid_node`;');
	$db->exec('TRUNCATE TABLE `xmlnodes`;');
	$db->exec('TRUNCATE TABLE `running_lock`');
	$db = NULL;
}
