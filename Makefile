MDS=$(wildcard *.md)
HTMLS=$(patsubst %.md,%.html,${MDS})

version.php:
	echo "<?php\ndefine('VERSION', '$$(git describe)');\ndefine('VERSION_COMMIT', '$$(git rev-parse HEAD)');\ndefine('VERSION_COMMIT_SHORT', '$$(git rev-parse --short HEAD)');\ndefine('VERSION_TIMESTAMP', '$$(date -Is)');\n?>" > $@

html: ${HTMLS}

%.html: %.md Makefile
	cat $< | python3 -m markdown -x toc -x tables -x fenced_code -x codehilite > $@

clean:
	rm ${HTMLS}

autogen:
	while true; do inotifywait *.md && make html; done

.PHONY: html clean autogen version.php
