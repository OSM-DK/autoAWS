-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 23, 2023 at 03:06 AM
-- Server version: 5.6.51-cll-lve
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autoaws`
--
CREATE DATABASE IF NOT EXISTS `autoaws` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `autoaws`;

-- --------------------------------------------------------

--
-- Table structure for table `add_addr`
--

CREATE TABLE `add_addr` (
  `osak:identifier` char(32) COLLATE utf8_bin NOT NULL,
  `lat` double UNSIGNED NOT NULL,
  `lon` double UNSIGNED NOT NULL,
  `addr:city` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `addr:country` char(2) COLLATE utf8_bin NOT NULL DEFAULT 'DK',
  `addr:housenumber` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `addr:postcode` int(4) UNSIGNED DEFAULT NULL,
  `addr:street` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `addr:municipality` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `addr:place` varchar(64) COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `awsdata`
--

CREATE TABLE `awsdata` (
  `osak:identifier` char(32) COLLATE utf8_bin NOT NULL,
  `lat` double UNSIGNED NOT NULL,
  `lon` double UNSIGNED NOT NULL,
  `postnummer_navn` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `husnr` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `postnummer_nr` int(4) UNSIGNED DEFAULT NULL,
  `vejstykke_navn` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `kommune_navn` varchar(32) COLLATE utf8_bin NOT NULL,
  `sup_bynavn` varchar(34) COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `delete_addr`
--

CREATE TABLE `delete_addr` (
  `node_id` bigint(12) UNSIGNED NOT NULL,
  `lat` double UNSIGNED DEFAULT NULL,
  `lon` double UNSIGNED DEFAULT NULL,
  `xml` text COLLATE utf8_bin
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `invalid_node`
--

CREATE TABLE `invalid_node` (
  `node_id` bigint(12) UNSIGNED NOT NULL,
  `lat` double UNSIGNED NOT NULL,
  `lon` double UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `last_update`
--

CREATE TABLE `last_update` (
  `postnummer` int(4) UNSIGNED NOT NULL,
  `opdateret` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ois_fixes`
--

CREATE TABLE `ois_fixes` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `fixed_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `mun_no` smallint(4) UNSIGNED NOT NULL,
  `str_no` smallint(4) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `osmdata`
--

CREATE TABLE `osmdata` (
  `osak:identifier` char(32) COLLATE utf8_bin NOT NULL,
  `node_id` bigint(12) UNSIGNED NOT NULL,
  `lat` double UNSIGNED NOT NULL,
  `lon` double UNSIGNED NOT NULL,
  `addr:city` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `addr:country` char(2) COLLATE utf8_bin NOT NULL DEFAULT 'DK',
  `addr:housenumber` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `addr:postcode` int(4) UNSIGNED DEFAULT NULL,
  `addr:street` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `addr:municipality` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `addr:place` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `autoaws_ignore` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `running_lock`
--

CREATE TABLE `running_lock` (
  `currently_locked` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `update_addr`
--

CREATE TABLE `update_addr` (
  `osak:identifier` char(32) COLLATE utf8_bin NOT NULL,
  `node_id` bigint(12) UNSIGNED NOT NULL,
  `lat` double UNSIGNED NOT NULL,
  `lon` double UNSIGNED NOT NULL,
  `addr:city` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `addr:country` char(2) COLLATE utf8_bin NOT NULL DEFAULT 'DK',
  `addr:housenumber` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `addr:postcode` int(4) UNSIGNED DEFAULT NULL,
  `addr:street` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `addr:municipality` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `addr:place` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `xml` text COLLATE utf8_bin
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `xmlnodes`
--

CREATE TABLE `xmlnodes` (
  `node_id` bigint(12) UNSIGNED NOT NULL,
  `xml` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_addr`
--
ALTER TABLE `add_addr`
  ADD PRIMARY KEY (`osak:identifier`);

--
-- Indexes for table `awsdata`
--
ALTER TABLE `awsdata`
  ADD PRIMARY KEY (`osak:identifier`);

--
-- Indexes for table `delete_addr`
--
ALTER TABLE `delete_addr`
  ADD PRIMARY KEY (`node_id`);

--
-- Indexes for table `invalid_node`
--
ALTER TABLE `invalid_node`
  ADD PRIMARY KEY (`node_id`);

--
-- Indexes for table `last_update`
--
ALTER TABLE `last_update`
  ADD PRIMARY KEY (`postnummer`);

--
-- Indexes for table `ois_fixes`
--
ALTER TABLE `ois_fixes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `osmdata`
--
ALTER TABLE `osmdata`
  ADD PRIMARY KEY (`osak:identifier`),
  ADD UNIQUE KEY `node_id` (`node_id`);

--
-- Indexes for table `running_lock`
--
ALTER TABLE `running_lock`
  ADD PRIMARY KEY (`currently_locked`);

--
-- Indexes for table `update_addr`
--
ALTER TABLE `update_addr`
  ADD PRIMARY KEY (`osak:identifier`),
  ADD UNIQUE KEY `node_id` (`node_id`);

--
-- Indexes for table `xmlnodes`
--
ALTER TABLE `xmlnodes`
  ADD PRIMARY KEY (`node_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ois_fixes`
--
ALTER TABLE `ois_fixes`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
