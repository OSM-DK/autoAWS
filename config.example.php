<?php
// Connection information for OSM
define('OSM_API_URL' , 'https://api.openstreetmap.org/api/0.6/');
require_once 'osm_api_oauth2_token.php'; // generate this using generate_oauth2_token.py

// Connection information for MySQL/MariaDB
define('MYSQL_HOST'    , 'localhost');
define('MYSQL_DB'      , 'autoaws');
define('MYSQL_USERNAME', '<db user>');
define('MYSQL_PASSWORD', '<db password>');

// Logging configuration
define('LOG_PATH'     , '.');
define('LOG_BASENAME' , 'autoaws');
define('LOG_COUNT'    , 20);
define('LOG_MAX_SIZE' , 5*1024*1024);
?>
